package ru.tsk.ilina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.command.AbstractProjectCommand;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.enumerated.Sort;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListShowCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all projects";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sort = TerminalUtil.nextLine();
        List<Project> projects;
        if (sort == null || sort.isEmpty()) projects = serviceLocator.getProjectService().findAll(userId);
        else {
            Sort sortValue = Sort.valueOf(sort);
            System.out.println(sortValue.getDisplayName());
            projects = serviceLocator.getProjectService().findAll(userId, sortValue.getComparator());
        }
        int index = 1;
        for (Project project : projects) {
            System.out.println(index + ". " + project.toString());
            index++;
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
