package ru.tsk.ilina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.command.AbstractTaskCommand;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.enumerated.Sort;
import ru.tsk.ilina.tm.model.Task;
import ru.tsk.ilina.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListShowCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all tasks";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sort = TerminalUtil.nextLine();
        List<Task> tasks;
        if (sort == null || sort.isEmpty()) tasks = serviceLocator.getTaskService().findAll(userId);
        else {
            Sort sortValue = Sort.valueOf(sort);
            System.out.println(sortValue.getDisplayName());
            tasks = serviceLocator.getTaskService().findAll(userId, sortValue.getComparator());
        }
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " + task.toString());
            index++;
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
