package ru.tsk.ilina.tm.api.repository;

import ru.tsk.ilina.tm.model.AbstractEntity;
import ru.tsk.ilina.tm.model.User;

import java.util.Comparator;
import java.util.List;

public interface IAbstractRepository<E extends AbstractEntity> {

    List<E> findAll();

    E add(E entity);

    void addAll(List<E> entities);

    E findById(String id);

    E removeById(String id);

    E remove(E entity);

    void clear();

}
