package ru.tsk.ilina.tm.api.repository;

import ru.tsk.ilina.tm.model.User;

import java.util.List;

public interface IUserRepository extends IAbstractRepository<User> {

    User findByEmail(String email);

    User removeByLogin(String login);

    User findByLogin(String login);

}
