package ru.tsk.ilina.tm.api.repository;

import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IAbstractBusinessRepository<Project> {

}
