package ru.tsk.ilina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractOwnerEntity extends AbstractEntity {

    @Nullable
    private String userId;

}
