package ru.tsk.ilina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.api.entity.IWBS;
import ru.tsk.ilina.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractBusinessEntity implements IWBS {

    @Nullable
    private String projectId = null;

}
