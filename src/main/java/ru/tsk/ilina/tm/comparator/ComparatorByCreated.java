package ru.tsk.ilina.tm.comparator;

import ru.tsk.ilina.tm.api.entity.IHasCreated;
import ru.tsk.ilina.tm.api.entity.IHasStartDate;

import java.util.Comparator;

public class ComparatorByCreated implements Comparator<IHasCreated> {

    private static final ComparatorByCreated INSTANCE = new ComparatorByCreated();

    public ComparatorByCreated() {
    }

    public static ComparatorByCreated getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(IHasCreated o1, IHasCreated o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getCreatedDate() == null || o2.getCreatedDate() == null) return 0;
        return o1.getCreatedDate().compareTo(o2.getCreatedDate());
    }

}
