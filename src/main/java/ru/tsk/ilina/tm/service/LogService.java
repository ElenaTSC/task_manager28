package ru.tsk.ilina.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.api.service.ILogService;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.*;

public final class LogService implements ILogService {

    private static final String COMMANDS = "COMMANDS";
    private static final String COMMANDS_FILE = "./commands.txt";
    private static final String ERRORS = "ERRORS";
    private static final String ERRORS_FILE = "./errors.txt";
    private static final String MESSAGES = "MESSAGES";
    private static final String MESSAGES_FILE = "./messages.txt";

    private static final String FILE_PROPERTY = "/logger.properties";

    private final LogManager manager = LogManager.getLogManager();
    private final Logger root = Logger.getLogger("");
    private final Logger errors = Logger.getLogger(ERRORS);
    private final Logger commands = Logger.getLogger(COMMANDS);
    private final Logger messages = Logger.getLogger(MESSAGES);

    private final ConsoleHandler consoleHandler = new ConsoleHandler();

    {
        init();
        registry(errors, ERRORS_FILE, true);
        registry(messages, MESSAGES_FILE, true);
        registry(commands, COMMANDS_FILE, false);
    }

    private void init() {
        final InputStream inputStream = LogService.class.getResourceAsStream(FILE_PROPERTY);
        try {
            manager.readConfiguration(inputStream);
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }

    private void registry(@NotNull final Logger logger, @NotNull final String fileName, @NotNull final boolean isConsole) {
        if (isConsole) logger.addHandler(consoleHandler);
        logger.setUseParentHandlers(false);
        try {
            logger.addHandler(new FileHandler(fileName));
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }

    public void info(@NotNull final String message) {
        if (message.isEmpty()) return;
        messages.info(message);
    }

    public void debug(@NotNull final String message) {
        if (message.isEmpty()) return;
        messages.fine(message);
    }

    public void command(@NotNull final String message) {
        if (message.isEmpty()) return;
        commands.info(message);
    }

    public void error(@NotNull final Exception e) {
        errors.log(Level.SEVERE, e.getMessage());
    }

}
