package ru.tsk.ilina.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.exception.AbstractException;

public class EmptyDomainException extends AbstractException {

    public EmptyDomainException() {
        super("Error! Domain is empty");
    }

    public EmptyDomainException(@NotNull final String message) {
        super("Error! " + message + " domain is empty");
    }

}
