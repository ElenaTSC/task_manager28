package ru.tsk.ilina.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error! Password is empty");
    }

    public EmptyPasswordException(@NotNull final String message) {
        super("Error! " + message + " password is empty");
    }

}
